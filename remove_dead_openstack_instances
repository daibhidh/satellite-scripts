#!/usr/bin/env ruby
#
# this script is provided as is with no warranty, implied or otherwise.  Use at your own risk.
# Please feel free to reuse this.
#
# Author: David Rose <david.rose@starnix.se>
#

##
# Array contianing the dependencies to be loaded in for this script.
dependencies = [ 'optparse', 'yaml', 'highline/import', 'time', 'logging', 'awesome_print', 'apipie-bindings', 'fog/openstack', 'resolv' ]

##
#require script deps, installing if not available.
# Look over loop.  In testing it does one dep at a time and the command has to be rerun before it does the next one.  should grab all at once.
#

dependencies.each do |dep|
  begin
    loaded = false
    begin
      require dep
      loaded = true
    rescue LoadError
      puts "Gem #{dep} not found.  Attempting to install it."
      if dep == 'highline/import'
        system("gem install highline")
      elsif dep == 'fog/openstack'
        system("gem install fog-openstack")
      else
        system("gem install #{dep}")
      end
      Gem.clear_paths
    end
    require dep unless loaded
    
  rescue => e
    puts "Something went wrong checking for dependencies. Exception below: \n\n"
    raise e
  end
end  

##
#Start of options parser
#
@options = { }

##
# Set default values
@defaults = {
  :sat_uri         => 'https://localhost',
  :sat_timeout     => 300,
  :sat_user        => 'admin',
  :sat_pass        => nil,
  :sat_org         => 1,
  :verify_ssl  => true,
  :debug       => false,
  :hammer_conf => "~/.hammer/cli_config.yml",
  :openstack_auth_url => nil,
  :openstack_username => nil,
  :openstack_api_key => nil,
  :openstack_domain_name => nil,
  :openstack_resource_file => nil,
  :limit => nil,
  :host_filter => 'name~floating-ip',
  :display_only => false,
}

##
# Create options list 
optparse = OptionParser.new do |opts|
  opts.banner = "Usage: #{opts.program_name} ACTION [options]"
  opts.version = "0.1"

  opts.on("-U", "--uri=URI", "URI to the Satellite") do |u|
    @options[:sat_uri] = u
  end
  opts.on("-t", "--timeout=TIMEOUT", OptionParser::DecimalInteger, "Timeout value in seconds for any API calls. -1 means never timeout") do |t|
    @options[:sat_timeout] = t
  end
  opts.on("-u", "--user=USER", "User to log in to Satellite") do |u|
    @options[:sat_user] = u
  end
  opts.on("-p", "--pass=PASS", "Password to log in to Satellite") do |p|
    @options[:sat_pass] = p
  end
  opts.on("-o", "--organization-id=ID", "ID of the Organization to manage CVs in") do |o|
    @options[:sat_org] = o
  end
  opts.on("--no-verify-ssl", "don't verify SSL certs") do
    @options[:verify_ssl] = false
  end
  opts.on("--debug", "Show debug statements for apipie") do
    @options[:debug] = true
  end
  opts.on("--hammer-conf FILE", "Hammer configuration file") do |file|
    @options[:hammer_conf] = file
  end
  opts.on("--openstack-rc-file FILE", "Openstack RC Environment file") do |file|
    @options[:openstack_resource_file] = file
  end
  opts.on("--openstack-uri=URI", "OpenStack Auth URI") do |u|
    @options[:openstack_auth_url] = u
  end
  opts.on("--openstack-username=user", "Username for OpenStack") do |v|
    @options[:openstack_username] = v
  end
  opts.on("--openstack-password=password", "Password for OpenStack ") do |v|
    @options[:openstack_api_key] = v
  end
  opts.on("--openstack-domain=domain", "Domain name for Openstack") do |v|
      @options[:openstack_domain_name] = v
  end
  opts.on("--host-filter filter", String, "Filter for finding Openstack Instances in Satellite") do |v|
    @options[:host_filter] = v
  end
  opts.on("--limit=num", Integer, "Limit host results to num results") do |v|
    @options[:limit] = v
  end
  opts.on("--display-only", "Do not change anything, just show what would be done (Without Rest debug)") do
    @options[:display_only] = true
  end
end

##
# Parse options
optparse.parse!

##
# Set default values for those that have not been set by options
@defaults.each do |key,val|
  if not @options.has_key?(key)
    @options[key] = val
  end
end

##
# Next 3 functions borrowed from https://stackoverflow.com/a/19826329

def bash_env(cmd=nil)
  env = `#{cmd + ';' if cmd} printenv`
  env.split(/\n/).map {|l| l.split(/=/)}
end

# Source a given file, and compare environment before and after.
#   Returns Hash of any keys that have changed.
def bash_source(file)
  Hash[ bash_env(". #{File.realpath file}") - bash_env() ]
end

# Find variables changed as a result of sourcing the given file, 
#   and update in ENV.
def source_env_from(file)
  bash_source(file).each {|k,v| ENV[k] = v }
end

# Source resource file and set environment variables
if @options[:openstack_resource_file]
  source_env_from(File.expand_path(@options[:openstack_resource_file]))
end

# If no options are given for openstack, try environment variables
begin
    @options[:openstack_username] ||= ENV['OS_USERNAME']
    @options[:openstack_api_key] ||= ENV['OS_PASSWORD']
    @options[:openstack_auth_url] ||= ENV['OS_AUTH_URL']
    @options[:openstack_domain_name] ||= ENV['OS_USER_DOMAIN_NAME']
rescue
  $stderr.puts "No Openstack settings specified and no OpenStack environment variables found"
  exit 1
end

##
# Check if hammer config exists.  If so, try to use it's values for connecting to the satellite.
if File.exists?(File.expand_path(@options[:hammer_conf]))
  config = YAML.load_file(File.expand_path(@options[:hammer_conf]))
  @options[:sat_user] = config[:foreman][:username]
  @options[:sat_pass] = config[:foreman][:password]
  @options[:sat_timeout] = config[:foreman][:request_timeout]
  @options[:sat_uri] = config[:foreman][:host]
end

##
# If not specified by command line options or hammer, prompt for user name and password.
# Ask for Satellite username and password
if not @options[:sat_user]
  @options[:sat_user] = ask('Satellite username: ')
end

if not @options[:sat_pass]
  @options[:sat_pass] = ask('Satellite password: ') { |q| q.echo = false }
end


if @options[:debug]
  @options[:display_only] = true
end

def connect_to_satellite
  # Set up the connection
  # Uses username and password
  if @options[:debug]
    @api = ApipieBindings::API.new( {:uri => @options[:sat_uri], :username => @options[:sat_user], :password => @options[:sat_pass], :api_version => '2', :timeout => @options[:sat_timeout], :logger => Logging.logger(STDOUT)}, {:verify_ssl => @options[:verify_ssl]})
  else
    @api = ApipieBindings::API.new({:uri => @options[:sat_uri], :username => @options[:sat_user], :password => @options[:sat_pass], :api_version => '2', :timeout => @options[:sat_timeout]}, {:verify_ssl => @options[:verify_ssl]})
  end
end

def get_hosts_by_filter(filter)
  if @options[:limit]
    return @api.resource(:hosts).call(:index, { :thin => true, :full_results => true, :search => "#{filter}"})['results'].first(@options[:limit])
  end
  return @api.resource(:hosts).call(:index, { :thin => true, :full_results => true, :search => "#{filter}"})['results']
end

def get_missing_hosts(sat_list, osp_list)
  return sat_list.map { |h| h['name'] } - osp_list.map { |i| i[:name] }
end

def remove_host_from_satellite(name)
  @api.resource(:hosts).call(:destroy, { :id => get_hosts_by_filter("name=#{name}")[0]['id'] })
end

def connect_to_openstack
  token ={:provider=>:openstack,
         :openstack_api_key=> @options[:openstack_api_key],
         :openstack_username=> @options[:openstack_username],
         :openstack_auth_url=> @options[:openstack_auth_url],
         :openstack_domain_name=> @options[:openstack_domain_name]
         }
  
  @osp ||= Fog::Compute.new(token)
end

@active_instances = [ ]

begin
  connect_to_satellite
  hosts = get_hosts_by_filter(@options[:host_filter])
  puts "Matched hosts in Satellite: #{hosts.count}"
  puts "#{hosts.map { |h| h['name'] }.join("\n")}" if @options[:display_only]
rescue => e
  puts "Error getting host list from Satellite.  Exception below:\n\n"
  raise e
end

begin  
  connect_to_openstack
  active_instances = @osp.list_servers_detail({:all_tenants => true }).body['servers'].map { |i| {:name => i['name'], :pub_interface => Resolv.getnames(i['addresses']['public'][0]['addr'])[0].to_s } unless i['status'] != 'ACTIVE' }.compact
  puts "Active instances in Openstack: #{active_instances.count}"
rescue => e
  puts "Something went wrong getting the Active instance list from Openstack. Exception Below:\n\n"
  raise e
end

missing = get_missing_hosts(hosts, active_instances)

puts "Number of hosts to be removed: #{missing.count}"

puts "#{missing.join('\n')}" if @options[:display_only]

begin
  unless missing.empty?
    missing.each do |h|
      puts "Removing host #{host.upcase}"
      remove_host_from_satellite(host) unless @options[:display_only] and not @options[:debug]
    end
  end
rescue => e
  puts "Something went wrong removing hosts from the Satellite.  Exception Below:\n\n"
  raise e
end




  
        
    
  
