#!/usr/bin/env ruby
#
# this script is provided as is with no warranty, implied or otherwise.  Use at your own risk.
# Please feel free to reuse this.
#
# Author: David Rose <david.rose@starnix.se>
#

##
# Array contianing the dependencies to be loaded in for this script.
dependencies = [ 'optparse', 'yaml', 'highline/import', 'time', 'logging', 'awesome_print', 'apipie-bindings', 'thread' ]


##
#require script deps, installing if not available.
# Look over loop.  In testing it does one dep at a time and the command has to be rerun before it does the next one.  should grab all at once.
#

dependencies.each do |dep|
  begin
    loaded = false
    begin
      require dep
      loaded = true
    rescue LoadError
      puts "Gem #{dep} not found.  Attempting to install it."
      if dep == 'highline/import'
        system("gem install highline")
      else
        system("gem install #{dep}")
      end
      Gem.clear_paths
    end
    require dep unless loaded
    
  rescue => e
    puts "Something went wrong checking for dependencies. Exception below: \n\n"
    raise e
  end
end  

##
#Start of options parser
#
@options = { }

##
# Set default values
@defaults = {
  :uri         => 'https://localhost',
  :timeout     => 300,
  :user        => 'admin',
  :pass        => nil,
  :verify_ssl  => true,
  :debug       => false,
  :hammer_conf => "~/.hammer/cli_config.yml",
  :subscription_types => [ "Linux Server", "Workstation" ],
  :threads => 10,
  :query => 'name~virt-who',
  :verbose => false,
}

##
# Create options list 
optparse = OptionParser.new do |opts|
  opts.banner = "Usage: #{opts.program_name} ACTION [options]"
  opts.version = "0.1"
  

  opts.on("-U", "--uri=URI", "URI to the Satellite") do |u|
    @options[:uri] = u
  end
  opts.on("-t", "--timeout=TIMEOUT", OptionParser::DecimalInteger, "Timeout value in seconds for any API calls. -1 means never timeout") do |t|
    @options[:timeout] = t
  end
  opts.on("-u", "--user=USER", "User to log in to Satellite") do |u|
    @options[:user] = u
  end
  opts.on("-p", "--pass=PASS", "Password to log in to Satellite") do |p|
    @options[:pass] = p
  end
  opts.on("-o", "--organization-id=ID", "ID of the Organization to manage CVs in") do |o|
    @options[:org] = o
  end
  opts.on("--no-verify-ssl", "don't verify SSL certs") do
    @options[:verify_ssl] = false
  end
  opts.on("--debug", "Show debug statements for apipie") do
    @options[:debug] = true
  end
  opts.on("--hammer-conf FILE", "Hammer configuration file") do |file|
    @options[:hammer_conf] = file
  end
  opts.on("--subscription-types PATTERN1,PATTERN2,...", Array, "Comma seperated list of Patterns to match.  One Pattern per subscription. Example: \"Linux Server\",\"Workstation\"") do |patterns|
    @options[:subscription_types] = patterns
  end
  opts.on("--threads NUM", Integer, "Number of threads to use") do |t|
    @options[:threads] = t
  end
  opts.on("-v PATTERN", "--virt-who-filter PATTERN", "Filter Pattern for finding virt-who nodes.  (Default = 'name~virt-who')") do |v|
    @options[:query] = v
  end
  opts.on("--verbose", "Show verbose output (I.E all hosts being checked instead of just ones being changed)") do
    @options[:verbose] = true
  end
end

##
# Parse options
optparse.parse!

##
# Set default values for those that have not been set by options
@defaults.each do |key,val|
  if not @options.has_key?(key)
    @options[key] = val
  end
end

##
# Check if hammer config exists.  If so, try to use it's values for connecting to the satellite.
if File.exists?(File.expand_path(@options[:hammer_conf]))
  config = YAML.load_file(File.expand_path(@options[:hammer_conf]))
  @options[:user] = config[:foreman][:username]
  @options[:pass] = config[:foreman][:password]
  @options[:timeout] = config[:foreman][:request_timeout]
  @options[:uri] = config[:foreman][:host]
end


##
# If not specified by command line options or hammer, prompt for user name and password.
# Ask for Satellite username and password
if not @options[:user]
  @options[:user] = ask('Satellite username: ')
end

if not @options[:pass]
  @options[:pass] = ask('Satellite password: ') { |q| q.echo = false }
end

def connect_to_api
  # Set up the connection
  # Uses username and password
  if @options[:debug]
    @api = ApipieBindings::API.new( {:uri => @options[:uri], :username => @options[:user], :password => @options[:pass], :api_version => '2', :timeout => @options[:timeout], :logger => Logging.logger(STDOUT)}, {:verify_ssl => @options[:verify_ssl]})
  else
    @api = ApipieBindings::API.new({:uri => @options[:uri], :username => @options[:user], :password => @options[:pass], :api_version => '2', :timeout => @options[:timeout]}, {:verify_ssl => @options[:verify_ssl]})
  end
end

def get_virt_who_nodes
  return @api.resource(:hosts).call(:index, {:full_results => true, :search => @options[:query]})['results'].map { |v| { :name => v['name'], :id => v['id'] } }
end

def fix_host_subscriptions(host)
  to_remove = [ ]
  subs = @api.resource(:host_subscriptions).call(:index, {:host_id => host['id']})['results']
  subs.each do |s|
    @options[:subscription_types].each do | term |
      if s['name'].include? term
        to_remove << s
      end
    end
  end
  if to_remove.size >= 1
    if @options[:noop]
      to_remove.each do |s|
        puts "Subscription to remove: #{s['name']}\tQuantity: #{s['quantity_consumed']}"
      end
    else
      puts "\t\t:: #{host['name']} :: Removing #{to_remove.size} subscriptions..."
      # Remove subscriptions
      @api.resource(:host_subscriptions).call(:remove_subscriptions, {:host_id => host['id'], :subscriptions => to_remove.map { |s| { 'id' => s['id'] } } })
      puts "\t\t:: #{host['name']} :: Triggering auto-attach..."
      #Trigger auto-attach
      @api.resource(:host_subscriptions).call(:auto_attach, {:host_id => host['id']})
    end
  end
end

def get_guests(vnode_id)
  return @api.resource(:hosts).call(:show, {:id => vnode_id})['subscription_facet_attributes']['virtual_guests']
end

##
# Beginning of script
#

connect_to_api

get_virt_who_nodes.each do |node|
  puts "Working on #{node[:name]} with id: #{node[:id]}" if @options[:debug]
  @guests = get_guests(node[:id])
  unless @guests.empty?
    until @guests.empty?
      num_threads = @options[:threads] <= @guests.count ? @options[:threads] : @guests.count
      @threads = [ ]
      num_threads.times do |i|
        @threads[i] = Thread.new {
          begin
            guest = @guests.pop
            puts "Processing:: #{guest}" if @options[:debug]
            puts "\n:: #{node[:name]} :: Checking #{guest['name']}..." if @options[:verbose]
            fix_host_subscriptions(guest)
          rescue => e
            puts "Error processing guest: #{guest['name']}"
            puts "Excpetion was:\n #{e}"
          end
        }
      end
      @threads.each { |t| t.join }
    end
    unless @threads.each.map { |t| t.alive? }.none?
      sleep 1
    end
  end
end


