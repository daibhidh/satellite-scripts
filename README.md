# Description
---------------
A Collection of scripts for use in Administrating a RedHat Satellite 6 server. 


*ALL scripts as placed here as is, with no guarantee or warrenty and may even be incomplete/broken*


## Scripts
| Name | Language | Hardcoded/Generic | Multi-Threaded |
|:-----|:------------|:--------------|:----------|
|maintenance-tasks | Bash | Hardcoded values | No |
|update_activationkey | Ruby | Generic | No |
|singulate_host_subscriptions | Ruby | Generic | Yes |
|singulate_virt-who_host_subscriptions | Ruby | Generic | Yes |
|remove_dead_openstack_instances | Ruby | Generic | No |

### maintenance-tasks
This script is meant to be run on the satellite server itself and
works well with cron.  There are several tasks that can be performed
which I find useful to have running.


#### Usage

Example Crontab:
```bash
[root@gotrhn ~]# crontab -l
00 13 * * */2 /root/repos/satellite/scripts/maintenance-tasks refresh 2>&1
00 09 * * */2 /root/repos/satellite/scripts/maintenance-tasks db_cleaning 2>&1
```

Usage/Help output:
```bash
./maintenance-tasks [reindex|refresh|db_cleaning|offline_db_cleaning]
 reindex                                                Reindex the katello database
 refresh                                                Refresh the subscription manifest
 db_cleaning                                            Online cleaning of DB backends
 offline_db_cleaning                                    Offline Cleaning of DB backends. This will also remove ALL foreman tasks
 clean_tasks <optional task type>                       Clean Tasks. DEfault, older then one week. Task types are: all, success, error, warning
```
### update_activationkey_subscription
This script checks the specified activation key and adds any missing
subscriptions of the given subscription filters to it. 

#### Usage

```bash
./update_activationkey_subscription 
Usage: update_activationkey_subscription [options]
    -U, --uri=URI                    URI to the Satellite
    -t, --timeout=TIMEOUT            Timeout value in seconds for any API calls. -1 means never timeout
    -u, --user=USER                  User to log in to Satellite
    -p, --pass=PASS                  Password to log in to Satellite
    -o, --organization-id=ID         ID of the Organization to manage CVs in
        --no-verify-ssl              don't verify SSL certs
        --debug                      Show debug statements for apipie
        --hammer-conf FILE           Hammer configuration file
        --activation-key NAME        Activation Key to modify
        --subscription-filters x,y,z Comma seperated list of subscription name search patterns
        --noop                       Do not change anything, only display what would be done
```

Eaxmple Usage:

```bash
./update_activationkey_subscription --no-verify-ssl --activation-key PhysicalSystem --subscription-filters "L
inux Server","Workstation","Developer Suite"
:: PhysicalSystem :: Sorting Subscription list for filter: Linux Server
:: PhysicalSystem :: 0 new subscriptions found.
:: PhysicalSystem :: Sorting Subscription list for filter: Workstation
:: PhysicalSystem :: 0 new subscriptions found.
:: PhysicalSystem :: Sorting Subscription list for filter: Developer Suite
:: PhysicalSystem :: 0 new subscriptions found.
```

### singulate_host_subscriptions
This script is meant to clean up host subscriptions caused by 'no
auto-attach' activation-keys and the chicken and egg problem with
activation keys.  The chicken/egg problem is, described using the new
Oracle Java subscriptions as an example, thus:

If you have the Oracle subscriptions all in one Activation Key (and we
where given several, but not grouped under one 'ID') and have
auto-attach enabled, the repositories are not available during
installation.  If you use an automated installtion, your screwed.
Ansible/Chef/Cfengine fail.  I suppose you could add in a lot of logic
to look for an available pool-id for one of those subscriptions, but I
went a different route.

No Autoattach on the key.  The problem here though is that every
system being registered will get 1 from each 'Subscription ID' in the
list.  In essense, it will take multiple subscriptions forn then same
product when it only needs/requires 1.

This script takes a host list obtained by a filter string, uses a name
search for subscriptions on that host, and removes the excess based on
the filter.  This script is multi-threaded and defaults to 10 threads.


#### Usage
```bash



./singulate_host_subscriptions --help
Usage: singulate_host_subscriptions ACTION [options]
    -U, --uri=URI                    URI to the Satellite
    -t, --timeout=TIMEOUT            Timeout value in seconds for any API calls. -1 means never timeout
    -u, --user=USER                  User to log in to Satellite
    -p, --pass=PASS                  Password to log in to Satellite
    -o, --organization-id=ID         ID of the Organization to manage CVs in
    -n, --noop                       do not actually execute anything
        --no-verify-ssl              don't verify SSL certs
        --host-filter FILTER         Filter for host selection
        --subscription-filter PATTERN
                                     Name filter for selecting Subscriptions (Subscription name search)
        --debug                      Show debug statements for apipie
        --display-only               Do not change anything, just show what would be done (Without Rest debug)
        --limit NUM                  Limit action to NUM hosts
```        
Usage Example:
 ```bash
./singulate_host_subscriptions --no-verify-ssl --subscription-filter "Oracle Java Add-On" --host-filter 'os=RedHat' --threads 50
Host filter matched: 541 host(s)
:: NODE1.EXAMPLE.COM :: 7 matching subscriptions
:: NODE2.EXAMPLE.COM :: 7 matching subscriptions
:: NODE28.EXAMPLE.COM :: 7 matching subscriptions
:: NODE37.EXAMPLE.COM :: 7 matching subscriptions
:: NODE37.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE2.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE28.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE1.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE12.EXAMPLE.COM :: 7 matching subscriptions
:: NODE40.EXAMPLE.COM :: 7 matching subscriptions
:: NODE42.EXAMPLE.COM :: 7 matching subscriptions
:: NODE45.EXAMPLE.COM :: 7 matching subscriptions
:: NODE100.EXAMPLE.COM :: 7 matching subscriptions
:: NODE123.EXAMPLE.COM :: 7 matching subscriptions
:: NODE111.EXAMPLE.COM :: 8 matching subscriptions
!!!!!! UNABLE TO PROCESS HOST :: node88.example.com !!!!!!
HTML/Rest ERROR: 400 Bad Request
:: NODE9.EXAMPLE.COM :: 7 matching subscriptions
:: NODE99.EXAMPLE.COM :: 7 matching subscriptions
:: NODE100.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE123.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE42.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE45.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE9.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE12.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE40.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
:: NODE111.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 449, 455, 458, 461]
:: NODE99.EXAMPLE.COM :: Removed the following Subscription ID's: 
[441, 442, 446, 455, 458, 461]
Done processing
```

### singulate_virt-who_host_subscriptions
This script will go through all 'virt-who-*' content hosts in the
satellite and look at associated system to it, removing 'non' VDC
related subscriptions from the content host.  Defaults to "Linux
Server" and "Workstation".


#### Usage
```bash
./singulate_host_subscriptions --help
Usage: singulate_host_subscriptions [options]
    -U, --uri=URI                    URI to the Satellite
    -t, --timeout=TIMEOUT            Timeout value in seconds for any API calls. -1 means never timeout
    -u, --user=USER                  User to log in to Satellite
    -p, --pass=PASS                  Password to log in to Satellite
    -o, --organization-id=ID         ID of the Organization to manage CVs in
        --no-verify-ssl              don't verify SSL certs
        --host-filter FILTER         Filter for host selection
        --subscription-filter PATTERN
                                     Name filter for selecting Subscriptions (Subscription name search)
        --debug                      Show debug statements for apipie
        --display-only               Do not change anything, just show what would be done (Without Rest debug)
        --limit NUM                  Limit action to NUM hosts
        --hammer-conf FILE           Hammer configuration file
        --verbose                    Verbose output (minus debug)
    -T, --threads NUM                Number of threads (default 10)
        --per-page NUM               Maximum number of hosts to retrieve.  (Default 1000)

```

Example Usage:
 ```bash
./singulate_virt-who_host-subscriptions --no-verify-ssl --subscription-types "Linux Server","Workstation"
:: virt-who-ovirt1.example.com-1 :: Checking webnode1.example.com...
:: virt-who-ovirt1.example.com-1 :: Checking webnode2.example.com...
:: virt-who-ovirt1.example.com-1 :: Checking dbnode5.example.com...
:: virt-who-ovirt1.example.com-1 :: Checking redis12.example.com... 

```

### remove_dead_openstack_instances
This script will connect to the specified Openstack Service (expects
list-all-tenants) to retrieve a list of 'ACTIVE' instances.  This list
is then cross referenced with a list of hosts obtained by a search
filter from the satellite.  Hosts in the list from the satellite that
do not exist in the active_instances list are removed from the
satellite.
    
Openstack credentials can be specified on the command line, an RC file
can be specified, or they can be obtained from already existing
environment variables.

Satellite credentials can be obtained on command line or via a hammer
config file.

#### Usage
```bash
root@23d8f9236e75:/project# ./remove_dead_openstack_instances --help
Usage: remove_dead_openstack_instances ACTION [options]
       -U, --uri=URI                    URI to the Satellite
       -t, --timeout=TIMEOUT            Timeout value in seconds for any API calls. -1 means never timeout
       -u, --user=USER                  User to log in to Satellite
       -p, --pass=PASS                  Password to log in to Satellite
       -o, --organization-id=ID         ID of the Organization to manage CVs in
           --no-verify-ssl              don't verify SSL certs
           --debug                      Show debug statements for apipie
           --hammer-conf FILE           Hammer configuration file
           --openstack-uri=URI          OpenStack Auth URI
           --openstack-username=user    Username for OpenStack
           --openstack-password=password
                                        Password for OpenStack
           --openstack-domain=domain    Domain name for Openstack
           --host-filter filter         Filter for finding Openstack Instances in Satellite
           --limit=num                  Limit host results to num results
           --display-only               Do not change anything, just show what would be done (Without Rest debug)
```
